﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour
{
    bool click = false;
    public TrailRenderer Tr;
    UnityEngine.Vector2 lastMousepos;
    float AxisX;
    float AxisY;
    float MousSpeed;
    int score = 0;
    public CircleCollider2D cc;
    public GameObject[] point;
    bool activ = true;
    int i=0;

    
    // Update is called once per frame
    void Update()
    {
        if (activ)
        {
            if (Input.GetMouseButtonDown(0))//ЛКМ нажата
            {
                click = true;
                cc.isTrigger = true;
            }

            if (Input.GetMouseButtonUp(0))//ЛКМ отпущена
            {
                click = false;
                cc.isTrigger = false;

                point[1].active = false;
                point[2].active = false;
                point[3].active = false;
                point[4].active = false;
                point[5].active = false;
                i = 0;
            }
        }
        

        Tr.emitting = click;

        UnityEngine.Vector3 temp = Input.mousePosition;
        temp.z = 10f;

        this.transform.position = Camera.main.ScreenToWorldPoint(temp);

        if (lastMousepos == UnityEngine.Vector2.zero)
        {
            lastMousepos = Input.mousePosition;
        }
        else
        {
            AxisX = ((Input.mousePosition.x - lastMousepos.x) / Time.deltaTime) / Screen.width;
            AxisY = ((Input.mousePosition.y - lastMousepos.y) / Time.deltaTime) / Screen.height;
            lastMousepos = Input.mousePosition;
        }
        MousSpeed = Math.Abs((AxisX + AxisY) / 2);
       

        
    }
    private void OnTriggerExit2D(Collider2D collision) //Проверка находится ли мышка на букве
    {
        if (collision.name == "BOXTEXTUR")
        {
            score++;
            Debug.Log(score);
        } 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == point[point.Length - 1].name)
        {
            finish();
        }
        else
        if (collision.name == point[i].name)
        {
            point[i+1].active = true;
            i++;
            Debug.Log(i);
            //Debug.Log(point.Length);
        }
        

    }

    public void start()
    {
        point[i + 1].active = true;
        i++;
    }
    void finish()
    {
        //Text text = PanelText.GetComponent<Text>();
        //Panel.active = true;
        click = false;
        cc.isTrigger = false;
        
        activ = false;
        
        if (score == 0)
        { Debug.Log("отлично"); }
        if (score == 1)
        {
            Debug.Log("сойдет");
        }
        if (score > 1)
            {
            Debug.Log("плохо");
        }
        Reset();
    }

    public void Reset()
    {
        Application.LoadLevel(Application.loadedLevelName);
    }
}
